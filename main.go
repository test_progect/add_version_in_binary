package main

import (
	"flag"
	"log"
)

var gitCommit string

func printVersion() {
	log.Printf("Current build version: %s", gitCommit)
}

func main() {
	versionFlag := flag.Bool("v", false, "Print the current version and exit")
	flag.Parse()

	switch {
	case *versionFlag:
		printVersion()
		return
	}
	// continue with other stuff
}
